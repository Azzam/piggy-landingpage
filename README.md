# Piggy Landing Page Demo

#### Author: Mohamed Azzam


## Installation


To install via npm:

```bash
$ npm install
$ bower install
```

To Run App and view Page index over your localhost

```bash
$ gulp serve
```

To Build Dist Folder

```bash
$ gulp build
```
